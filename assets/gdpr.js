export const gdpr = `<h1> Klaunka Pastelka </h1>

<p> Ochrana osobních údajů na webu www.klaunkapastelka.cz </p>
<p> Podnikatelský subjekt Veronika Folířová., IČO: 05133564, se sídlem  Mozartova 7 , 73601 Havířov (dále jen „dodavatel“ nebo „správce“) zpracovává ve smyslu nařízení Evropského parlamentu a Rady (EU) č. 2016/679 o ochraně fyzických osob v souvislosti se zpracováním osobních údajů a o volném pohybu těchto údajů a o zrušení směrnice 95/46/ES (obecné nařízení o ochraně osobních údajů) (dále jen „Nařízení“), následující osobní údaje: </p>
  <ul>
    <li> jméno, příjmení </li>
    <li> e-mailovou adresu </li>
    <li> telefonní číslo </li>
    <li> adresu/sídlo </li>
    <li> číslo objednávky, smlouvy </li>
  </ul>

<p> Výše uvedené osobní údaje je nutné zpracovat pro odbavení objednávek a další plnění ze smlouvy, pokud mezi vámi a poskytovatelem dojde k uzavření smlouvy. Takové zpracování osobních údajů umožňuje čl. 6 odst. 1 písm. b) Nařízení – zpracování je nezbytné pro splnění smlouvy. </p>

<p> Poskytovatel zpracovává tyto údaje rovněž za účelem evidence smlouvy a případného budoucího uplatnění a obranu práv a povinností smluvních stran. Uchování a zpracování osobních údajů je za výše uvedeným účelem po dobu nejdéle 10 let od realizace poslední části plnění dle smlouvy, nepožaduje-li jiný právní předpis uchování smluvní dokumentace po dobu delší. Takové zpracování je možné na základě čl. 6 odst. 1 písm. c) a f) Nařízení – zpracování je nezbytné pro splnění právní povinnosti a pro účely oprávněných zájmů správce. </p>

<p> Zpracování osobních údajů je prováděno dodavatelem, tedy správcem osobních údajů. </p>
<p> Předávání osobních údajů třetím subjektům. Takto předané údaje zahrnují především vaše jméno a příjmení, adresu konání akce, tel. číslo, na kterém vás může umělec kontaktovat, a pokud  nebyla faktura uhrazena předem, tak i případně částku, kterou je třeba před  začátkem akce uhradit. Umělec  se ve vztahu k osobním údajům, které mu předáme, stává správcem pro účely splnění objednané služby. </p>

<p> V těchto případech předáváme vaše osobní údaje třetím subjektům: </p>
    <ul>
      <li> Poskytovatel služby - umělec </li>
    </ul>

<p> Správce osobních údajů, jakožto provozovatel webové stránky www.klaunkapastelka.cz, užívá na této webové stránce soubory cookies, které jsou zde užity za účelem: </p>
  <ul>
    <li> měření návštěvnosti webových stránek a vytváření statistik týkající se návštěvnosti a chování návštěvníků na webových stránkách </li>
    <li> základní funkčnosti webových stránek </li>
  </ul>

<p> Vezměte, prosíme, na vědomí, že podle Nařízení máte právo: </p>
<ul>
  <li> vznést námitku proti zpracování na základě oprávněného zájmu správce, </li>
  <li> požadovat po nás informaci, jaké vaše osobní údaje zpracováváme, </li>
  <li> vyžádat si u nás přístup k těmto údajům a tyto nechat aktualizovat nebo opravit, popřípadě požadovat omezení zpracování, </li>
  <li> požadovat po nás výmaz těchto osobních údajů, výmaz provedeme, pokud to nebude v rozporu s platnými právními předpisy nebo oprávněnými zájmy správce, </li>
  <li> na přenositelnost údajů, pokud se jedná o automatizované zpracování na základě souhlasu nebo z důvodu plnění smlouvy, </li>
  <li> požadovat kopii zpracovávaných osobních údajů, </li>
  <li> na účinnou soudní ochranu, pokud máte za to, že vaše práva podle Nařízení byla porušena v důsledku zpracování vašich osobních údajů v rozporu s tímto Nařízením, </li>
  <li> podat stížnost u Úřadu pro ochranu osobních údajů. </li>
</ul>

<p> Tyto Zásady ochrany osobních údajů jsou účinné od 25. 5. 2018 </p>
`
