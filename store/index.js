export default {
  state () {
    return {
      nabidka: ['Klaunský program', 'Malování na obličej', 'Balónková show', 'Mikulášský program', 'Karneval', 'Balónková výzdoba'],
      kamaradi: ['Holandský billiard', 'Kouzelnické vystoupení mistra ČR', 'Kouzelnický workshop pro děti', 'Laserová střelnice', 'Lukostřelba', 'Mikromagie mistra ČR', 'Moderní magická show']
    }
  }
}
