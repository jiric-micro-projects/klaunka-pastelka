/**
* Defines globally used components so we dont have to import them all the time.
* Use this also for mixins, directives, etc..
*/

import Vue from 'vue'

import ImgShow from '~/components/b-img-show.vue'
import ImgGallery from '~/components/gallery.vue'

Vue.component(ImgShow.name, ImgShow)
Vue.component(ImgGallery.name, ImgGallery)
