
export const baseUrl = 'https://klaunkapastelka.cz'

export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: 'Klaunka pastelka',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Klaunka pastelka' },
      { hid: 'keywords', name: 'keywords', content: 'Klaunka pastelka, Bublinkování, Balónková show, Mikulášský program' },
      { hid: 'author', name: 'author', content: 'www.binaro.cz' },
      { hid: 'robots', name: 'robots', content: 'all' },

      // favicon
      { name: 'msapplication-TileColor', content: '#eb0097' },
      { name: 'theme-color', content: '#eb0097' },

      // og tags
      { hid: 'og:title', property: 'og:title', content: 'Klaunka pastelka' },
      { hid: 'og:site_name', property: 'og:site_name', content: 'Klaunka pastelka' },
      { hid: 'og:description', property: 'og:description', content: 'Klaunka pastelka' },
      { hid: 'og:image', property: 'og:image', content: `${baseUrl}/logo.png` },

      // compatibility
      {'http-equiv': 'X-UA-Compatible', content: 'IE=edge'},
    ],
    link: [
      // icons
      { rel: 'apple-touch-icon', sizes: '180x180', href: '/apple-touch-icon.png' },
      { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/favicon-32x32.png' },
      { rel: 'icon', type: 'image/png', sizes: '16x16', href: '/favicon-16x16.png' },
      { rel: 'icon', type: 'image/png', sizes: '192x192', href: '/android-icon-192x192.png' },
      { rel: 'manifest', href: '/site.webmanifest' },
      { rel: 'mask-icon', href: '/safari-pinned-tab.svg', color: '#eb0097' },
      { href: 'https://fonts.googleapis.com/css?family=Indie+Flower&display=swap', rel: 'stylesheet' },
    ],
    script: [
      // IE polyfill
      { src:'https://polyfill.io/v3/polyfill.js?features=es5,es6,es7&flags=gated', type: 'application/javascript' },
    ],
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#eb0097' },
  /*
  ** Global CSS
  */
  css: [
    { src: 'assets/icons/style.css' },
    { src: 'assets/scss/override.scss' },
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: 'plugins/init.js' },
    { src: 'plugins/components.js' }
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/sitemap',
  ],

  axios: {
    baseURL: baseUrl,
  },

   /*
  ** Sitemap module configuration
  ** See https://github.com/nuxt-community/sitemap-module
  */
 sitemap: {
    hostname: baseUrl,
  },

  generate: {
    fallback: '404.html'
  },

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // Doc: https://bootstrap-vue.js.org/docs/
    'bootstrap-vue/nuxt',
  ],

  bootstrapVue: {
    bootstrapCSS: false,
    bootstrapVueCSS: false,
    componentPlugins: ['LayoutPlugin', 'FormPlugin', 'FormTextareaPlugin', 'FormCheckboxPlugin', 'FormInputPlugin', 'ModalPlugin'],
    components: ['BImg', 'BButton', 'BAlert', 'BCarousel', 'BCarouselSlide', 'BNav', 'BNavItem'], // Here you can specify which components you want to load and use
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
